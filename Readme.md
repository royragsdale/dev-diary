# Dev Daily

[![pipeline
status](https://gitlab.com/royragsdale/dev-daily/badges/master/pipeline.svg)](https://gitlab.com/royragsdale/dev-daily/commits/master)

Inspired by a combination of Dean Wilson's [blog post][bp] of the same name,
Daniel Reeves of Beeminder's [1000 Days of User-Visible Improvements][uvi] and
the mentioned Paul Graham's ["How Not To Die"][pg].

## Content

A daily log of development related things:

- I learned
- I didn't understand

> Startups rarely die in mid keystroke. So keep typing!

[bp]:https://www.unixdaemon.net/career/developer-diaries-and-til/
[uvi]:http://messymatters.com/uvi/
[pg]:http://www.paulgraham.com/die.html
