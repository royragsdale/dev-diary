---
title: "Return"
date: 2020-01-04T17:00:31-05:00
tags: [hugo, bash, u-root, chromeos]
---

You know it's been a while when you try and hop on to make a post and your
deploy is broken, your theme is out of date, and in general it takes about as
much time just getting something live as you planned to spend actually adding
some content.

## Broken deploy

The crux of the issue here was building the site locally with an older version
of `hugo` than was being used in the [GitLab CI pipeline][ci] (not pinned). The
fix I went with was updating locally and keeping CI at latest. As an aside,
there sure are a lot of ways to [install][i] hugo these days. For maximum
resiliency I could also pin the CI container version, or use the same container
locally, but for now this works.

As it turns out the behavior was not entirely as a result of the hugo version it
was really the combination of the current hugo with an old version of the theme
I am using. Updating the theme fixed this, but that was another patch of git
submodules and figuring out how I had the theme setup.

In the end it's still a really nice setup in that I can write and publish all
with just a push. That's pretty low friction.

## New Name

Introducing *Dev Daily*.

This used to be title "Dev Log", but the repository was titled `dev-diary`,
which always sort of rubbed me wrong. Now I've standardized everything as
`dev-daily`.

Here is an `grep`, `xargs`, `sed` one liner to update all the links in all my
posts:

```
grep -lr "dev-diary" | xargs -I {} sed -i 's/dev-diary/dev-daily/g' {}
```

Small thing, but nice to have it resolved.

[ci]:https://gitlab.com/royragsdale/dev-daily/blob/master/.gitlab-ci.yml#L2
[i]:https://gohugo.io/getting-started/installing

## The Main Event

New year, no better time to jump back in and make this habit. When I've spent
the time to write here I've personally found it really valuable so getting back
on that proverbial horse. This year I am going to try something new. Picking one
area/project and following it more deeply with an aim of actually producing
something. This is in contrast to my perpetual tinkering, and previously aimless
walk through interesting tech.

The specific area that has caught my mind recently is the **low level boot
process, firmware, and related software ecosystem**. I like this because there
is a ton I will need to learn in this area, and while it has security
implications it is somewhat of a builder type focus, rather than a "find all the
bugs" breaker focus. I'd like to build more things.


### Motivation

Part of what has sparked this interest is following the creation of the [Oxide
Computer Company][o] and listening to their [On the Metal][otm] podcast. So
many interesting things going on there. 

Another element which sparked my interest was getting a new laptop, the Star
Labs [Star Lite Mk II][s]. This is a vendor who ships Linux laptops, and
provides the feature that 

> The BIOS, Embedded Controller and SSD receive automatic firmware updates via
> the LVFS. 

I was not previously familiar with the [Linux Vendor Firmware Service
(LVFS)][lvfs] or the `fwupdmgr` command, but it is awesome. Think `apt update`
for your chips! How much easier is that than the old way of having to use
a windows install, or extract bits from an EXE and build a live usb. Very cool.

The final bit was having a Chromebook which had unfortunately reached End of
Life. Though it works perfectly fine for daily browsing, it's not getting
updates and so it's not the kind of device I want to put my main google account
on. I've always thought Chromebooks are the model for future computing with
automatic updates, strong cryptographic guarantees for boot, and a focus on
security. The only reason I got the Star Lite rather than a Chromebook was the
small friction associated with either crouton or the crostini environments.

In sum, these 3 components are what have set me down this path.

- [Ron Minnich][rm] talking about [webboot][] and the potential for self-signed
  [verified boot][vb] with the NiChrome project.
- Wanting to learn more about [coreboot][], but it not currently being available
  for my new machine.
- Having an EOL Chromebook to experiment with.

[o]:https://oxide.computer/
[otm]:https://oxide.computer/blog/categories/on-the-metal/
[s]:https://us.starlabs.systems/pages/star-lite
[lvfs]:https://fwupd.org/
[rm]:https://oxide.computer/blog/on-the-metal-3-ron-minnich/
[webboot]:https://github.com/u-root/webboot
[vb]:https://github.com/u-root/NiChrome/blob/master/verified_boot.md
[coreboot]:https://www.coreboot.org/

### Plan

So that's what I'm going to do. In even just my first glance there are a ton of
resources to get up to speed on. Since they aren't as easily consolidated and
discoverable as one might hope, I'm going to try and approach this as a way of
documenting them as well.

This may be a bit of a link dump, and follows my personal discovery path rather
than perhaps a real hierarchy, if it makes sense I'll eventually organize it.

#### [NiChrome][ni]

> An open ChromeOS distro with a Go-based userland, X11, and upspin support.
> (WIP)

Last active August 2019, last/only "release" `v1.0.0 (2018-10-01)`. Interesting
because it documents some steps to [Re-key your Chromebook and return to
Verified mode][rk].

Searching for more information on the current state of NiChome yielded the first
real break in helpful information, which was somehow this [tweet][] pointing
towards a few relevant Google Documents:

- [Be your own vendor:Build your own ChromeOS distro and image server][bb]
- [Break your chromebook! Or, rather, install your own chromium OS, built from
scratch...][cc]

And most helpful of all, the "Open Source Firmware" slack,
[u-root.slack.com][slack]. Unfortunately scroll back/archive of slack is no
particularly friendly but it's a start.

[tweet]:https://twitter.com/hugelgupf/status/969000772363931648
[bb]:https://docs.google.com/presentation/d/1jSUJteAjEgHCFyx6VsqhWmNGTKipvTmAdsoW0gme7qA/edit#slide=id.p
[cc]:https://docs.google.com/document/d/1VBLVWlFTyt0oWJu9sCZ1_Sd70FqcqBc3nkJz2VXPg28/edit#heading=h.cvelmwx5zyp
[slack]:u-root.slack.com

#### [webboot][]

> Tools to let a u-root instance boot signed live distro images over the web

This is related in that the kernel "is built using a config originally from
NiChromeOS".  Also it seems more actively updated and is mentioned in the slack
so could be usable in a real sense, or at least enough to experiment with.

#### [u-root][ur]

> A fully Go userland with Linux bootloaders! u-root can create a root file
> system (initramfs) containing a busybox-like set of tools written in Go. 

Both of these projects fall under the `u-root` org.

## Onward!

Good day in terms of finding resources. Certainly a stack of things to triage
and get through. Excited for this year.


[ni]:https://github.com/u-root/NiChrome
[rk]:https://github.com/u-root/NiChrome/blob/master/verified_boot.md#part-3-re-key-your-chromebook-and-return-to-verified-mode
[ur]:http://u-root.tk/
